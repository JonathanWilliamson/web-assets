# Blender Web Assets
Blender Web Assets is a front-end framework which provides design and interactivity components for official [blender.org](http://www.blender.org) websites.

## Install
### Requirements
* **[git](https://git-scm.com)** to clone and contribute to this repository.
* **[node.js](https://nodejs.org)** to install the necessary packages via `npm`. If you need to manage multiple node versions, [nvm](https://github.com/nvm-sh/nvm) can be quite handy.

### Setup
1. Open a terminal.
2. Clone repo: `git clone git@projects.blender.org:infrastructure/web-assets.git`.
3. Enter folder `cd /path/to/web-assets`.
4. `npm install`

## Usage
### Standalone
1. `npm start`. This will build the site into `/public`, open the site in the browser, and watch for changes in .html and .sass files.


### In other projects
The idea behind Web Assets is to be shared between official Blender projects. Using this repository as a **git submodule** can simplify the process.

Clone, initalize, and update:
1. `git clone git@projects.blender.org:infrastructure/web-assets.git`
2. `git submodule init`
3. `git submodule update --remote`

Then in each project you can just `@import` or `@use` via Sass the components you need.

Each project must have its own `package.json` defining the build pipeline.

## Demo
See Web Assets in action:

* [blender.org](https://www.blender.org)
* [Developer Blog](https://code.blender.org) (extending the blender.org theme)
* [Blender ID](https://id.blender.org)
* [Blender Conference](https://conference.blender.org)
* [Blender Open Data](https://opendata.blender.org/)

## Authors
[Pablo Vazquez](https://projects.blender.org/pablovazquez)
[Niklas Ravnsborg-Gjertsen](https://projects.blender.org/niklasravnsborg)

## Copyright and license
This project is licensed under [the GPL license](LICENSE) copyright © 2023 Blender Foundation.
