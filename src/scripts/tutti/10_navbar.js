/* Show/hide navbar on scroll
 * modified from http://jsfiddle.net/mariusc23/s6mLJ/31/
*/

let didScroll;
let lastScrollTop = 0;
const hideScrollThreshold = 100;
const navBar = document.querySelector('nav.navbar-primary');
const navBarHeight = navBar ? navBar.offsetHeight : 48;

window.addEventListener('scroll', () => {
  didScroll = true;
});

function hasScrolled() {
  if (!navBar) {
    return;
  }

  let st = window.pageYOffset || document.documentElement.scrollTop;

  // Only hide past a threshold.
  if (Math.abs(lastScrollTop - st) <= hideScrollThreshold) {
    return;
  }

  if (st > lastScrollTop && st > navBarHeight) {
    // Scrolling Down
    document.body.classList.add('is-navbar-hidden');
    navBar.classList.remove('navbar-shown');
    navBar.classList.add('navbar-hidden');
  } else {
    // Scrolling up
    document.body.classList.remove('is-navbar-hidden');
    navBar.classList.remove('navbar-hidden');
    navBar.classList.add('navbar-shown');
  }

  lastScrollTop = st;
}

setInterval(() => {
  if (didScroll) {
    hasScrolled();
    didScroll = false;
  }
}, 250);


/* Simple dropdown toggle.
 * Used on navbar mobile toggle and other dropdowns. */
function dropdownToggle() {
  let dropdownToggleBtn = this;
  let dropdownMenuName;
  let dropdownMenuItem;

  if (dropdownToggleBtn.hasAttribute('data-toggle-menu-id')) {
    dropdownMenuName = dropdownToggleBtn.getAttribute('data-toggle-menu-id');
    dropdownMenuItem = document.getElementById(dropdownMenuName);
  } else {
    dropdownMenuItem = dropdownToggleBtn.nextElementSibling;
  }

  if (!dropdownMenuItem) {
    return;
  }

  dropdownToggleBtn.classList.toggle('active');
  dropdownMenuItem.classList.toggle('is-visible');
}

window.onload = function(e) {
  const dropdownToggles = document.querySelectorAll('.js-dropdown-toggle, .js-show-toggle');

  for (var i = 0; i < dropdownToggles.length; i++) {
    dropdownToggles[i].addEventListener('click', dropdownToggle, false);
  }

  /* Close all menus, to prevent multiple menus open at the same time. */
  document.onclick = function (e) {
    const targetMenuId = e.target.getAttribute('data-toggle-menu-id');
    const targetMenuElement = document.getElementById(targetMenuId);

    /* If the clicked element is not a menu, or it's not the menu
     * that we are trying to open, close it. */
    if (!e.target.classList.contains('.js-dropdown-toggle') || targetMenuElement && targetMenuElement.classList.contains('is-visible')) {
      const dropdownMenus = document.querySelectorAll('.dropdown-menu, .js-dropdown-menu');

      for (var i = 0; i < dropdownMenus.length; i++) {
        if (dropdownMenus[i].id && dropdownMenus[i].id != targetMenuId) {
          dropdownMenus[i].classList.remove('is-visible', 'active');
        }
      }

      for (var i = 0; i < dropdownToggles.length; i++) {
        if (dropdownMenus[i].id && dropdownMenus[i].id != targetMenuId) {
          dropdownToggles[i].classList.remove('is-active', 'active');
        }
      }
    };
  }

  /* Show the jump-to-top button. */
  let hop = document.getElementById("hop");
  if (hop !== null) {
    hop.onclick = function (e) { window.scrollTo(0, 0); }
  };

  /* Add the 'is-scrolled' class to body on scroll. Used for styling. */
  window.addEventListener("scroll", function () {
    if (window.pageYOffset > 250) {
      document.body.classList.add("is-scrolled")
    } else {
      document.body.classList.remove("is-scrolled")
    }
  });
};
